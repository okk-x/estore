const menu = document.querySelector(".menu--desktop");
const overlay = document.querySelector(".overlay");
menu.addEventListener('mouseover', function () {
    overlay.style.display = "block";
});
menu.addEventListener('mouseout', function () {
    overlay.style.display = "none";
});
