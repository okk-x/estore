(function($){
    $(".register-form").on('submit',function(event){
        event.preventDefault();
        let error=false;
        let data={};
        let parentElement=$(this).parent();
        $(".register-form input").each(function(index,item){
            if(item.value.length==0)error=true;
            let name=item.getAttribute('name'),
                value=item.value;
            data[name]=value;

        });

        if(!error){
            const url='check-'+location.pathname.replace('/','');
            $.ajax({
                url:url,
                type:'POST',
                dataType:'json',
                data:JSON.stringify(data),
                success:function(response){
                    if(response['status']==='success'){
                        window.location.href="/account/";
                    }
                    else if(response['status']==='error'){
                        let html="<ul class=\"errors\">";
                        response['message'].forEach(function(message){
                            html+=`<li>${message}</li>`;
                        });
                        html+="</ul>";
                        $('.errors').remove();
                        $(html).insertBefore($(".register-form"));
                    }
                }
            });
        }
    })
})(jQuery);