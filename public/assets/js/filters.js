
jQuery(document).ready((function ($) {
    let data = new Array;
    let page, params, path = location.pathname.split("/")[1],referer;
    let links = [];

    data.push({
        'name': 'sort',
        'value': $('#sort-products select :selected').val()
    });


    currentPage();
    loadProducts();
    function currentPage(){
        let result;
        if(result=location.pathname.match(/([\d]+)/)){
            page=result[1];
        }
        else page=1;
    }
    function pagination(countPages) {
        let html = "";
        if (countPages > 1) {
            html += "<ul>";
            for (let i = 1; i <= countPages; i++) {
                if (i == page) {
                    html += '<li class="active">';
                } else {
                    html += '<li>';
                }
                if (i == 1) {
                    html += '<a href="/' + path + '">';
                } else {
                    html += '<a href="/' + path + '/page-' + i + '">';
                }
                html += i + '</a></li>';
            }
            html += "</ul>";
            $('.pagination').html(html);
            $('.pagination a').each(function () {
                links.push({
                    obj: this,
                    page: $(this).text()
                });
            });
        } else {
            $('.pagination').html('');
        }
    }


    function loadProducts() {
        $.ajax({
            url: "/" + path + "/page-" + page + "?filter",
            data: 'json_request=' + JSON.stringify(data),
            type: 'POST',
            cache: false,
            dataType: "json",
            success: function (response,textStatus,request) {
                if(response['message']===undefined) {
                    $(".category__main-content .message").remove();
                    if(!$('.pagination').length)$(".category__main-content").append('<div class="pagination"></div>');
                    __render(response);
                }
                else{
                    $(".product-card").remove();
                    $(".pagination").remove();
                    $(".category__main-content").append('<div class="message">'+response['message']+'</div>')
                }
            }
        });
    }

    function __render(products) {
        params = products.pop();
        $('#range-price-from').val(params['minPrice']);
        $('#range-price-to').val(params['maxPrice']);
        let html = "";
        products.forEach(function (product) {
            html += '<div class="product-card" data-id="'+product['id']+'"><a href="/product/' + product['id'] + '">' +
                '<img src="/public/assets/img/products/' + product['title-image'] + '" alt="" class="product-image"> ' +
                '<p class="product-title">' + product['title'] + '</p></a>' +
                '<span class="product-price">' + product['price'] + '<span class="currency">₴</span> </span>' +
                '<div class="product-card__action">' +
                '<button class="to-cart"><svg><use xlink:href="#cart"></use> </svg> <span>Купить</span> </button>' +
                '<button class="to-compare"> <svg> <use xlink:href="#compare"></use> </svg> </button>' +
                '</div> </div>';
        });
        $(".product-list").html(html);
        if(Number(page)>Number(params['countPages'])){
            history.replaceState(null,null,"/"+path+"/page-"+params['countPages']);
            page=params['countPages'];
        }
        pagination(params['countPages']);
    }

    $(window).on("popstate", function (event) {
        let result;
        if (result = location.pathname.match(/page-([\d]+)/)) {
            if (result[1] !== undefined) page = result[1];
        } else page = 1;
        loadProducts();
    });

    $('body').on("click", '.pagination a', function (event) {
        event.preventDefault();
        $(".pagination li").removeClass("active");
        $(this).addClass("active");
        let item;
        if (item = links.find(item => item.obj == this)) {
            page = item.page;
        }
        let url = "/" + path;
        if (page > 1) url += "/page-" + page;
        history.pushState(null, null, url);
        loadProducts();
    });

    $(".filter-itm input:checkbox").on("change", function () {
        $("#range-form input").removeClass("error");
        $('#range-form button').attr('disabled', false);

        let parent = $(this).parents(".filter-itm");
        let value, name, item;
        if (parent.attr('id') == 'brand') {
            name = parent.attr('id');
            value = $(this).val();
        } else {
            name = parent.find(".filter-title").text();
            value = $(this).siblings(".filter-itm-title").text();
        }

        if (this.checked) {
            if ((item = data.find(item => item['name'] === name)) !== undefined) {
                item['value'].push(value);
            } else {
                data.push({
                    'name': name,
                    'value': [value]
                });
            }
        } else {
            for (let i = 0; i < data.length; i++) {
                if (data[i]['name'] == name) {
                    {
                        item = data[i];
                        for (let j = 0; j < item['value'].length; j++) {
                            if (item['value'][j] === value) {
                                item['value'].splice(j, 1);
                                if (!item['value'].length) {
                                    delete item;
                                    data.splice(i, 1);
                                }
                            }
                        }

                    }
                }
            }
        }
        loadProducts();
    });

    $("#range-form input").on("input", function () {
        let number = parseInt($(this).val());
        if (isNaN(number)) number = 0;
        $(this).val(number);
        if ((number < params['minPrice'] || number > params['maxPrice']) ||
            (($(this).attr('id') === 'range-price-from') && number > params['maxPrice']) ||
            (($(this).attr('id') === 'range-price-to') && number < params['minPrice'])) {
            $(this).addClass("error");
            $('#range-form button').attr('disabled', true);
        } else {
            $(this).removeClass("error");
            $('#range-form button').attr('disabled', false);
        }
    });
    $('#range-form').on("submit", function (e) {
        e.preventDefault();
        let elem;
        if ((elem = data.find(elem => elem['name'] == 'range')) !== undefined) {
            elem['value'] = [$("#range-price-from").val(), $("#range-price-to").val()];
        } else {
            data.push({
                'name': 'range',
                'value': [$("#range-price-from").val(), $("#range-price-to").val()],
            });
        }
        loadProducts();
    });

    $("#sort-products select").on("change", function () {
        let elem;
        if ((elem = data.find(elem => elem['name'] == 'sort')) !== undefined) {
            elem['value'] = $('#sort-products select :selected').val();
        } else {
            data.push({
                'name': 'sort',
                'value': $('#sort-products select :selected').val()
            });
        }
        loadProducts();
    });


})
(jQuery));


