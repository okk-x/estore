(function ($) {
        let rating=null;
        let errors=[];

        $(".product-details__action a").on('click', function (event) {
            event.preventDefault();
            $(this).parent().addClass('active');
            $(this).parent().siblings().removeClass('active');
            const desc = $(".product-details__description");
            const rev = $(".product-details__reviews");
            if ($(this).text() === "Характеристики") {
                desc.css('display', 'block');
                rev.css('display', 'none');
            } else if ($(this).text() === "Отзывы") {
                rev.css('display', 'block');
                desc.css('display', 'none');
            }
        });

        $(".rating_block input").on('click', function () {
            rating = $(this).val();
        });

        $(".review-form").on('submit', async function (event) {
            event.preventDefault();
            $("ul.errors").remove();
            let text = $(".review-form textarea").val().trim();
            if (rating) {
                let data = {
                    rating,
                    product: window.location.href.match(/([\d]+)$/)[0],
                };
                if (text !== '') data.text = text;
                await getReviews(data);
            }
            else errors.push('Вы не поставили оценку товару');
            if(errors.length){
                let html='<ul class="errors">';
                errors.map(error=>{html+='<li>'+error+'</li>';});
                html+='</ul>';
                $(html).insertAfter('.review-form');
            }
            errors=[];
            rating=null;
        });
        function resetForm(){
            $('review-form textarea').val('');
        }
        async function getReviews(data){
            await $.post({
                url: "/review/create",
                data: JSON.stringify(data),
                dataType: 'json',
                contentType: 'application/json',
                success: function (response){
                    console.log(response.length);
                    if(response['message']===undefined&&response.length) updateReviews(response);
                    else errors.push(response['message']);
                }
            });
        }
        function updateReviews(review) {
            let html = '';
            for (let i = 0; i < review.length; i++) {
                html += '<div class="review-block">' +
                    '<div class="username">' + review[i]['name'] + '</div>' +
                    '<div class="rating-block">';
                for (let j = 0; j < 5; j++) {
                    if (review[i]['rating']) {
                        html += '<div class="rating">' +
                            '<svg class="active">' +
                            '<use xlink:href="#star"></use>' +
                            '</svg>' +
                            '</div>';
                        review[i]['rating']--;
                    } else {
                        html +=
                            '<div class="rating">' +
                            '<svg class="inactive">' +
                            '<use xlink:href="#star"></use>' +
                            '</svg>' +
                            '</div>';
                    }
                }
                html += '</div>';
                if (review[i]['text'] !== null) {
                    html += '<p class="review-text">' + review[i]['text'] + '</p>';
                }
                html += ' <span>' + review[i]['publication_date'] + '</span></div>'
            }
            //if(!$('.reviews-section__body').length)$('div.reviews-section__body').insertAfter($('.reviews-section__header'));
            $('.reviews-section__body').html(html);
            console.log('here');
        }

    }

)(jQuery);