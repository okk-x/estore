(function ($) {

    let count;
    let valueInput;
    initCart();

    function initCart() {
        $.post("/cart/count", {}, counter);
    }

    function counter(response) {
        let result = JSON.parse(response);
        count = result['total_count'];
        if (count > 0) {
            if (!$(".cart .button-counter").length)
                $(".cart").prepend('<div class="button-counter">' + count + '</div>');
            else {
                $(".cart .button-counter").text(count);
            }
        }
    }

    function checkRange(inputElement) {
        const min = parseInt(inputElement.attr('min')) || 1;
        const max = parseInt(inputElement.attr('max')) || 1;
        let value = parseInt(inputElement.val()) || min;
        if (value < min) value = min;
        if (value > max) value = max;
        inputElement.val(value);
        return valueInput=value;
    }

    $(".quantity button").on("click", function () {

        let td = $(this).parents('tr').children('td'),
            id = td.first().find("a").attr('data-id'),
            input = td.filter(":nth-child(4)").find('.quantity input'),
            number = 1, countProduct = parseInt(input.val());
        if ($(this).attr('id') == 'minus') {
            if (countProduct <= 1) return;
            number = -number;
            countProduct += number;
        } else if ($(this).attr('id') == 'plus') {
            countProduct++;
        }
        input.val(countProduct);

        checkRange(input);
        if((countProduct - input.val())>0){
            number=0;
        }

        if (number) {
            $.post("/cart/number/" + id + "/" + number, {}, counter);

            let price = parseInt(td.filter(":nth-child(3)").text());
            let totalPrice = parseInt(td.filter(":nth-child(5)").text());
            td.filter(":nth-child(5)").text(totalPrice + price * number + " ₴");
            $(".total span").text(parseInt($(".total span").text()) + price * number + " ₴");
        }
    });
    $(".quantity input").keydown(function () {
        valueInput=parseInt($(this).val());
        if(isNaN(valueInput))valueInput=1;
    });
    $(".quantity input").keyup(function () {

        let id =  $(this).parents('td').siblings().first().find("a").attr('data-id');
        let primary=valueInput,
            secondary=checkRange($(this))-primary;
        if(secondary) $.post("/cart/number/" + id + "/" + secondary, {}, counter);
    });


    $(".cart-section .delete").on("click", function () {
        let elem = $(this).parents('tr');
        $(".total span").text(parseInt($(".total span").text())-parseInt(elem.find('td').eq(4).text()) + " ₴");
        let id = $(this).parents('td').siblings().first().find("a").attr('data-id');
        $.post("/cart/remove/" + id, function (response) {
            counter(response);
            if (!count) location.reload()
            else elem.remove();

        })
    });
    $(".clear-cart").on("click", function () {
        $.post('/cart/clear/', function (response) {
            location.reload();
        })
    });
    $("body").on("click", ".to-cart", function () {
        let id = $(this).parents(".product-card").attr("data-id") ||
            location.pathname.match(/product\/([\d]+)/)[1];
        if (id) $.post("/cart/add/" + id, {}, counter);
    });

})(jQuery);






