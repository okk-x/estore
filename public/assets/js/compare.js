(function($){

    let count;
    initCompare();

    function initCompare() {
        $.post("/compare/count", {}, counter);
    }

    function counter(response) {
        //console.log(response);
        let result = JSON.parse(response);
        count = result['count'];
        if (count > 0) {
            if (!$(".compare .button-counter").length)
                $(".compare").prepend('<div class="button-counter">' + count + '</div>');
            else {
                $(".compare .button-counter").text(count);
            }
        }
    }
    $(".compare-section .delete").on("click", function () {

        let id = $(this).siblings().attr('data-id');
        console.log(id);
        $.post("/compare/delete/" + id, function (response) {
            counter(response);
            location.reload();
        })
    });
    $("body").on("click", ".to-compare", function () {
        let id = $(this).parents(".product-card").attr("data-id") ||
            location.pathname.match(/product\/([\d]+)/)[1];
        if (id) $.post("/compare/add/" + id, {}, counter);
    });
})(jQuery);