<?php

namespace App\Helpers;

class Validation
{
    public static function checkName($name){
        if(preg_match('/^[\w]{2,15}$/ui',$name))return true;
        return false;
    }
    public static function checkEmail($email){
        if(filter_var($email,FILTER_VALIDATE_EMAIL))return true;
        return false;
    }
    public static function checkPassword($password){
        if(preg_match('/^[\dA-Za-z!@#$%^&*]{6,}$/',$password))return true;
        return false;
    }
    public static function checkPhone($phone){
        if(preg_match('/^(\+380)?[\d]{9}$/',$phone))return true;
        return false;
    }
}