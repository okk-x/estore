<div class="hierarchy-categories">
    <ul>
        <li><a><?= $product['title'] ?></a></li>
        <li><a href="/<?= $category['slug'] ?>"><?= $category['title'] ?></a></li>
    </ul>
</div>

<div class="product-about">
    <div class="product-images">
        <img src="/public/assets/img/products/<?= $product['title-image'] ?>">
    </div>
    <div class="product-info">
        <h2><?= $product['title'] ?></h2>
        <div class="product-rating">
            <div class="rating-block">
                <?php for ($i = 0; $i < 5; $i++) {
                    if ($product['rating']) {
                        ?>
                        <div class="rating">
                            <svg class="active">
                                <use xlink:href="#star"></use>
                            </svg>
                        </div>
                        <?php $product['rating']--;
                    } else { ?>
                        <div class="rating">
                            <svg class="inactive">
                                <use xlink:href="#star"></use>
                            </svg>
                        </div>
                    <?php }
                } ?>
            </div>
            <div class="count-reviews"><span><?=$count ?> отзыв(-ов)</span></div>
        </div>
        <?php if ($product['available']) { ?>
            <div class="available active"><span>Есть</span>в наличии</div>
        <?php } else { ?>
            <div class="available inactive"><span>Нет</span>в наличии</div>
        <?php } ?>
        <div class="price-block">Стоимость:<span><?= $product['price'] ?></span><span class="currency">₴</span>
        </div>
        <div class="product-card__action">
            <button class="to-cart">
                <svg>
                    <use xlink:href="#cart"></use>
                </svg>
                <span>Купить</span>
            </button>
            <button class="to-compare">
                <svg>
                    <use xlink:href="#compare"></use>
                </svg>
            </button>
        </div>
    </div>
    <div class="product-details">
        <ul class="product-details__action">
            <li class="active"><a href="">Характеристики</a></li>
            <li><a href="">Отзывы</a></li>
        </ul>
        <div class="product-details__description">
            <h3>Характеристики <?= $product['title'] ?></h3>
            <div class="table-characteristic">
                <?php foreach ($description as $desc): ?>
                    <div class="row-description">
                        <div><?= $desc[0] ?></div>
                        <div class="delimeter"></div>
                        <div><?= $desc[1] ?></div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="product-details__reviews" style="display:none">
            <div class="reviews-section">
                <div class="reviews-section__header">
                    <? if ($isGuest) { ?>
                        <div>
                            <p>Авторизуйтесь, чтобы оставить отзыв!</p>
                            <a href="/login">Авторизоваться</a>
                        </div>
                    <? } else { ?>
                        <h3>Оставить отзыв</h3>
                        <div>
                            <span>Оцените товар</span>
                            <div class="rating_block">
                                <input type="radio" name="rating" value="5" id="rating_5">
                                <label for="rating_5">
                                    <svg>
                                        <use xlink:href="#star"></use>
                                    </svg>
                                </label>
                                <input type="radio" name="rating" value="4" id="rating_4">
                                <label for="rating_4">
                                    <svg>
                                        <use xlink:href="#star"></use>
                                    </svg>
                                </label>
                                <input type="radio" name="rating" value="3" id="rating_3">
                                <label for="rating_3">
                                    <svg>
                                        <use xlink:href="#star"></use>
                                    </svg>
                                </label>
                                <input type="radio" name="rating" value="2" id="rating_2">
                                <label for="rating_2">
                                    <svg>
                                        <use xlink:href="#star"></use>
                                    </svg>
                                </label>
                                <input type="radio" name="rating" value="1" id="rating_1">
                                <label for="rating_1">
                                    <svg>
                                        <use xlink:href="#star"></use>
                                    </svg>
                                </label>
                            </div>
                        </div>
                        <form action="POST" class="review-form">
                            <textarea rows="5" placeholder="Ваш отзыв..."></textarea>
                            <button type="submit">Отправить</button>
                        </form>
                    <? } ?>
                </div>
                <div class="reviews-section__body">
                    <? foreach ($reviews as $review): ?>
                        <div class="review-block">
                            <div class="username"><?= $review['name'] ?></div>
                            <div class="rating-block">
                                <?php for ($i = 0; $i < 5; $i++) {
                                    if ($review['rating']) {
                                        ?>
                                        <div class="rating">
                                            <svg class="active">
                                                <use xlink:href="#star"></use>
                                            </svg>
                                        </div>
                                        <?php $review['rating']--;
                                    } else { ?>
                                        <div class="rating">
                                            <svg class="inactive">
                                                <use xlink:href="#star"></use>
                                            </svg>
                                        </div>
                                    <?php }
                                } ?>
                            </div>
                            <? if (isset($review['text'])) { ?>
                                <p class="review-text"><?= $review['text'] ?></p>
                            <? } ?>
                            <span><?= $review['publication_date'] ?></span>
                        </div>
                    <? endforeach; ?>
                </div>
                <? if ($count > 10) { ?>
                    <div class="show-more">
                        <span class="link-dashed">Показать ещё</span>
                    </div>
                <? } ?>
            </div>
        </div>
    </div>
</div>
