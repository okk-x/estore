<div class="login-page">
    <span>Вход в личный кабинет</span>
    <form class="register-form" action="/login" method="POST">
        <input type="text" name="email" placeholder="Email" required>
        <input type="password" name="password" placeholder="Пароль" min="6" required>
        <button class="submit" name="submit">Войти</button>
        <p class="message">Еще не зарегистрированы? <a href="/register">Зарегистрируйся!</a></p>
    </form>
</div>