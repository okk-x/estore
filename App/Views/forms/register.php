<div class="register-page">
    <span>Регистрация</span>
    <form class="register-form" action="/register" method="POST">
        <input type="text" name="first_name" placeholder="Имя" required>
        <input type="text" name="email" placeholder="Email" required>
        <input type="text" name="phone" placeholder="Номер телефона" required>
        <input type="password" name="password" placeholder="Пароль" required>
        <input type="password" name="re-password" placeholder="Повторите пароль" required>
        <button type="submit" class="submit"  name="submit">Зарегистрироваться</button>
        <p class="message">Уже зарегистрированы? <a href="/login">Войдите!</a></p>
    </form>
</div>