<div class="order-page">
    <? if(isset($orderId)){?>
        <div class="alert-info">Ваш заказ  №<?=$orderId?> был отправлен на обработку</div>
    <?} else{?>
    <h3>Оформление заказа</h3>
    <form method="POST" action="/checkout" class="order-form">
        <? if (isset($errors)) { ?>
            <ul class="errors">
                <? foreach ($errors as $error): ?>
                    <li><?= $error ?></li>
                <? endforeach; ?>
            </ul>
        <? } ?>
        <input type="text" name="first_name" placeholder="Имя" <? if ($auth) {
            echo 'readonly=""';
        } ?> value="<?= $data['first_name'] ?>" required>
        <input type="text" name="phone" placeholder="Номер телефона" <? if ($auth) {
            echo 'readonly=""';
        } ?> value="<?= $data['phone'] ?>" required>
        <button type="submit">Подтвердить заказ</button>
    </form>
    <div class="order-info">
        <ul>
            <? foreach ($products as $product) { ?>
                <li>
                    <a href="/<?= $product['id'] ?>"><img
                                src="/public/assets/img/products/<?= $product['title-image'] ?>"></a>
                    <div><a href=""><?= $product['title'] ?></a>
                        <span><?= $product['count_products'] ?> шт</span>
                        <span><?= $product['price'] * $product['count_products'] ?> ₴</span>
                    </div>
                </li>
            <? } ?>
        </ul>
        <div class="total"><span>Итого</span><span><?= $total ?> ₴</span></div>
    </div>
    <?}?>
</div>