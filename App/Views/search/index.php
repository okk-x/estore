<div class="search-page">
    <h2>Результаты по запросу «<?= $query ?>»</h2>
    <div class="product-list">
        <?php foreach ($products as $product): ?>
            <div class="product-card" data-id="<?= $product['id'] ?>">
                <a href="product/<?= $product['id'] ?>">
                    <img src="/public/assets/img/products/<?= $product['title-image'] ?>" alt=""
                         class="product-image">
                    <p class="product-title"><?= $product['title'] ?></p>
                </a>
                <span class="product-price"><?= $product['price'] ?> <span class="currency">₴</span> </span>
                <div class="product-card__action">
                    <button class="to-cart">
                        <svg>
                            <use xlink:href="#cart"></use>
                        </svg>
                        <span>Купить</span>
                    </button>
                    <button class="to-compare">
                        <svg>
                            <use xlink:href="#compare"></use>
                        </svg>
                    </button>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
