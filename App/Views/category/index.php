<div class="hierarchy-categories">
    <ul>
        <?php foreach ($categories as $category): ?>
            <li><a href="<?= $category['slug']; ?>"><?= $category['title']; ?></a></li>
        <?php endforeach ?>
    </ul>
</div>
<div class="category">
    <div class="category__filters">
        <div class="filter-itm" id="range-price">
            <span>Цена</span>
            <form id="range-form">
                <label for="range-price-from">от</label>
                <input type="text" id="range-price-from">
                <label for="range-price-to">до</label>
                <input type="text" id="range-price-to">
                <button type="submit">Установить диапазон</button>
            </form>
        </div>
        <div class="filter-itm" id="brand">
            <p class="filter-title">Бренды</p>
            <div class="filter-list">
                <?php foreach ($brands as $brand): ?>
                    <div><input type="checkbox" value="<?= $brand['id'] ?>"><span class="checkmark"></span><span
                                class="filter-itm-title"><?= $brand['title'] ?></span>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
        <?php foreach ($filters as $filter) { ?>
            <div class="filter-itm">
                <p class="filter-title"><?= $filter['title'] ?></p>
                <div class="filter-list">
                    <?php for($i=0;$i<count($filter['value']);$i++){ ?>
                        <div><input type="checkbox"><span class="checkmark"></span><span
                                    class="filter-itm-title" data-id="<?=$filter['id'][$i]?>"><?= $filter['value'][$i] ?></span></div>
                    <? } ?>
                </div>
            </div>
        <?php } ?>
        <div class="filter-footer"></div>
    </div>
    <div class="category__main-content">
        <div class="category-heading">
            <h2><?= $categories[0]['title']; ?></h2>
            <div id="sort-products">
                <span>Сортировка</span>
                <select>
                    <option value="popular">Популярные</option>
                    <option value="desc">От дорогих к дешевым</option>
                    <option value="asc">От дешевых к дорогим</option>
                </select>
            </div>
        </div>
        <div class="product-list">
        </div>
    </div>
</div>
