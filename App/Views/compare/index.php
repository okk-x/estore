<div class="compare-section">
    <span>Сравнение товаров</span>
    <table>
        <thead>
        <tr>
            <td>
                <div>
                    <div class="select-category">
                        <span>Выберите категорию:</span>
                        <ul>
                            <?php foreach ($list as $item): ?>
                                <li><a href="/compare/<?= $item['id'] ?>/1" <? if (isset($item['isCurrent'])) {
                                        echo 'class="current"';
                                    } ?>><?= $item['category'] ?>(<?= $item['count'] ?>)</a></li>
                            <? endforeach ?>
                        </ul>
                    </div>
                    <a href="/compare/clear" class="clear-compare">Очистить список</a>
                    <div class="switch-block">
                        <? if (isset($prev)) { ?>
                            <a href="/compare/<?= ($id . "/" . $prev) ?>"
                               class="btn-switch">&lt;&lt;&nbsp;предыдущие</a>
                        <? }
                        if (isset($next)) { ?>
                            <a href="/compare/<?= ($id . "/" . $next) ?>" class="btn-switch">&nbsp;следующие&nbsp;&gt;&gt;</a>
                        <? } ?>
                    </div>
                </div>
            </td>
            <?php foreach ($products as $product): ?>
                <td>
                    <button class="delete">
                        <svg>
                            <use xlink:href="#trash"></use>
                        </svg>
                    </button>
                    <a href="/product/<?= $product['id'] ?>" data-id="<?= $product['id'] ?>"><img
                                src="/public/assets/img/products/<?= $product['title-image'] ?>" alt="">
                        <span><?= $product['title'] ?></span>
                    </a></td>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($common as $characteristic): ?>
            <tr>
                <td><?= $characteristic ?></td>
                <?php foreach ($products as $product): ?>
                    <td><?php if (isset($product['description'][$characteristic])) {
                            echo $product['description'][$characteristic];
                        } else echo '—' ?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>