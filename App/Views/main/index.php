
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <a href="/product/1"><img class="d-block w-100" src="/public/assets/img/posters/galaxy-a51.jpg"></a>
        </div>
        <div class="carousel-item">
            <a href="/product/60"><img class="d-block w-100" src="/public/assets/img/posters/lxpic1_072918.jpg"></a>
        </div>
    </div>
</div>
<div class="main-page-rubric" id="main-page-rubric-sales-leaders">
    <h2>Популярные</h2>
    <div class="product-list">
        <?php foreach ($products as $product): ?>
            <div class="product-card" data-id="<?= $product['id'] ?>">
                <a href="product/<?= $product['id'] ?>">
                    <img src="/public/assets/img/products/<?= $product['title-image'] ?>" alt="" class="product-image">
                    <p class="product-title"><?= $product['title'] ?></p>
                </a>
                <span class="product-price"><?= $product['price'] ?> <span class="currency">₴</span> </span>
                <div class="product-card__action">
                    <button class="to-cart">
                        <svg>
                            <use xlink:href="#cart"></use>
                        </svg>
                        <span>Купить</span>
                    </button>
                    <button class="to-compare">
                        <svg>
                            <use xlink:href="#compare"></use>
                        </svg>
                    </button>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
</div>
<?php if (isset($viewed)){ ?>
<div class="main-page-rubric" id="main-page-rubric-recently-viewed-item">
    <h2>Недавно просмотренные товары</h2>
    <div class="product-list">
        <?php foreach ($viewed as $view): ?>
            <div class="product-card" data-id="<?= $view['id'] ?>">
                <a href="product/<?= $view['id'] ?>">
                    <img src="/public/assets/img/products/<?= $view['title-image'] ?>" alt="" class="product-image">
                    <p class="product-title"><?= $view['title'] ?></p>
                </a>
                <span class="product-price"><?= $view['price'] ?> <span class="currency">₴</span> </span>
                <div class="product-card__action">
                    <button class="to-cart">
                        <svg>
                            <use xlink:href="#cart"></use>
                        </svg>
                        <span>Купить</span>
                    </button>
                    <button class="to-compare">
                        <svg>
                            <use xlink:href="#compare"></use>
                        </svg>
                    </button>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php } ?>
</div>
