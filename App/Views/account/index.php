<div class="account-page">
    <div class="sidebar">
        <ul>
            <li><a href="" class="active">
                    <svg>
                        <use xlink:href="#list"></use>
                    </svg>
                    <span>Список заказов</span>
                </a></li>
        </ul>
    </div>
    <div class="account-main-section list-orders">
        <?php if(is_array($orders)&&count($orders)){?>
            <h3>Список заказов</h3>
            <table>
                <thead>
                <tr>
                    <td>Номер заказа</td>
                    <td>Товары</td>
                    <td>Стоимость</td>
                    <td>Статус</td>
                    <td>Дата заказа</td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($orders as $order): ?>
                    <tr>
                        <td><?= $order['id'] ?></td>
                        <td class="products">
                            <? foreach($order['products'] as $product):?>
                                <a href="/product/<?= $product['id'] ?>"><?=$product['title'] ?> X <?=$product['count_products']?></a>
                            <? endforeach;?>
                        </td>
                        <td><?=$order['total']?> ₴</td>
                        <td>
                            <? if(!strcmp($order['status'],'pending')){
                                echo "В обработке";
                            }else if(!strcmp($order['status'],'complete')){
                                echo "Получен";
                            }
                            ?>
                        </td>
                        <td>
                            <?
                            $date=new \DateTime($order['create_at']);
                            echo $date->format("d.m.Y")?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?} else{?>
            <div class="order-message">
                <h3>Список заказов пуст!</h3>
                <p>После оформления заказа, информацию о нём можно будет посмотреть здесь</p>
                <a href="/">Вернуться на главную</a>
            </div>
        <?}?>
    </div>
</div>
