<div class="menu--desktop">
    <div class="menu--title">
        <span>Каталог</span>
    </div>
    <div class="drop-list">
        <ul>
            <?php foreach($categories as $category):?>
            <li><a href="/<?=$category['slug']?>"><?=$category['title']?></a></li>
            <?php endforeach;?>
        </ul>
    </div>
</div>