<div class="cart-section">
    <? if (isset($products) && is_array($products)) { ?>
        <span>Корзина</span>
        <span class="clear-cart">Очистить корзину</span>
        <table>
            <thead>
            <tr>
                <td colspan="2">Наименование</td>
                <td>Стоимость</td>
                <td>Количество</td>
                <td>Общая стоимость</td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $product): ?>
                <tr>
                    <td><a href="/product/<?= $product['id'] ?>" data-id="<?= $product['id'] ?>"><img
                                    src="/public/assets/img/products/<?= $product['title-image'] ?>" alt="">
                        </a></td>
                    <td><a href="/product/<?= $product['id'] ?>"><?= $product['title'] ?></a></td>
                    <td><?= $product['price'] ?> ₴</td>
                    <td>
                        <div class="quantity">
                            <button id="minus" class="input-control"><span>&ndash;</span></button>
                            <input type="text" value="<?= $product['count_products'] ?>" min="1"
                                   max="<?= $product['number'] ?>">
                            <button id="plus" class="input-control"><span>+</span></button>
                        </div>
                    </td>
                    <td><?= $product['price'] * $product['count_products'] ?> ₴</td>
                    <td>
                        <button class="delete">
                            <svg>
                                <use xlink:href="#trash"></use>
                            </svg>
                        </button>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="total">
            <p>Итого: <span><?= $total ?> ₴</span></p>
            <button><a href="/checkout">Оформить заказ</a></button>
        </div>
    <? } else { ?>
        <div class="cart-message">
            <img src="/public/assets/img/cart-empty.png">
            <div>
                <h3>Корзина пуста!</h3>
                <a href="/">Вернуться на главную</a>
            </div>
        </div>
    <? } ?>
</div>
