<?php

namespace App\Controllers;

use App\Models\Category;
use App\Models\Product;
use Core\Controller;

final class CategoryController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->view->addScript('filters.js');
    }

    public function actionIndex($params)
    {
        $modelProduct = new Product;
        $modelCategory = new Category;

        list($slug, $page) = $params;
        $categories = [];

        if (!$currentCategory = $modelCategory->getCategoryBySlug($slug)) {
            throw new \Exception("Category not exists");
        }
        $brands = $modelProduct->getBrandsByCategory($currentCategory['id']);
        $filters = $modelCategory->getFilters($currentCategory['id']);
        $this->view->render("category/index", compact('categories', 'subcategories', 'brands', 'filters'));
    }

    public function actionFilter($params)
    {
        if (isset($_POST['json_request'])) {
            list($slug, $page) = $params;
            $product = new Product;
            $category = new Category;

            $filters = json_decode($_POST['json_request'], true);
            $description = [];
            $sort = null;
            $limit = 20;
            if ($category_id = $category->getCategoryBySlug($slug)['id']) $product->categoryId($category_id);
            $product->limit($limit);

            foreach ($filters as $filter) {
                if (!strcmp($filter['name'], 'sort')) {
                    switch ($filter['value']) {
                        case 'popular':
                            $sort = Product::SORT_POPULAR;
                            break;
                        case 'desc':
                            $sort = Product::SORT_DESC;
                            break;
                        case "asc":
                            $sort = Product::SORT_ASC;
                            break;
                    }
                } else if (!strcmp($filter['name'], "brand")) {
                    $brand_id = $filter['value'];
                    $product->brandId($brand_id);
                } else if (!strcmp($filter['name'], "range")) {
                    $product->range($filter['value'][0], $filter['value'][1]);
                } else {
                    $product->description($filter);
                }
            }

            $extra = $product->extraParams();
            if ($extra['countPages'] < $page) $page = $extra['countPages'];
            if($page<=0)$page=1;
            $product->offset(($page - 1) * $limit);
            if ($result = $product->getProducts($sort)) {
                array_push($result, $extra);
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
            }
            else echo json_encode(['message'=>'Нет товаров с такими характеристиками'],JSON_UNESCAPED_UNICODE);
        }
    }


}