<?php


namespace App\Controllers;
use App\Models\Product;
use Core\Controller;

final class SearchController extends Controller
{
    private $product;

    public function __construct()
    {
        parent::__construct();
        $this->product=new Product();
    }

    public function actionIndex($param){
        $query=htmlspecialchars(urldecode(trim($param[0])));
        if($products=$this->product->searchForTitle($query)){
            $this->view->render('search/index',compact('products','query'));
        }else
            $this->view->render('search/not-found',compact('query'));

    }

}