<?php


namespace App\Controllers;

use App\Helpers\Validation;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Core\Controller;


final class CartController extends Controller
{
    private $cart;

    public function __construct()
    {
        parent::__construct();
        $this->cart = new Cart();
        $this->checkAccessForCheckout();
    }

    public function checkAccessForCheckout()
    {
        $request = trim($_SERVER['REQUEST_URI'], '/');
        if (!$this->cart->count()['total_count'] && !strcmp($request, 'checkout')) header('Location:/login');
    }

    private function productsAndTotalPrice()
    {
        if ($this->cart->count()['total_count']) {
            $modelProduct = new Product;
            $result = $this->cart->productsInCart();
            $id = [];
            foreach ($result as $res) {
                $id[] = $res['product_id'];
            }
            $modelProduct->id($id);
            $products = $modelProduct->getProducts();
            $total = 0;

            for ($i = 0; $i < count($products); $i++) {
                $products[$i]['count_products'] = $result[$i]['count_products'];
                $total += $products[$i]['price'] * $products[$i]['count_products'];
            }
            return compact('products', 'total');
        }
        return false;
    }

    public function actionIndex()
    {
        if (!$data = $this->productsAndTotalPrice()) $data = [];
        $this->view->render("cart/index", $data);
    }

    public function actionAdd($id)
    {
        $this->cart->addProduct($id[0]);
        echo json_encode($this->cart->count());
    }

    public function actionProducts()
    {
        $modelProduct = new Product;
        if ($products = $this->cart->productsInCart()) {
            $id = [];
            foreach ($products as $product) {
                $id[] = $product['product_id'];
            }
            $modelProduct->id($id);
            $result = $modelProduct->getProducts();
            for ($i = 0; $i < count($result); $i++) {
                $result[$i]['count_products'] = $products[$i]['count_products'];
            }
            echo json_encode($result, JSON_UNESCAPED_UNICODE);
        }
    }

    public
    function actionCount()
    {
        echo json_encode($this->cart->count());
    }

    public
    function actionRemove($id)
    {
        $this->cart->removeProduct($id[0]);
        echo json_encode($this->cart->count());
    }

    public
    function actionClear()
    {
        $this->cart->clearCart();
    }

    public
    function actionChangeNumber($param)
    {
        list($id, $number) = $param;
        $this->cart->changeNumberProduct($id, $number);
        echo json_encode($this->cart->countProducts());
    }

    public function actionCheckout()
    {
        $user = new User();
        if ($id = $user->checkLogged()) {
            $data = $user->getUserData($id);
        }

        $errors = [];
        if (!empty($_POST)) {
            if (!isset($_POST['first_name']) || !isset($_POST['phone'])) {
                $errors[] = "Не все поля введены";
            }

            if (!Validation::checkName($_POST['first_name'])) {
                $errors[] = "Неправильно введено имя";
            }

            if (!Validation::checkPhone($_POST['phone'])) {
                $errors[] = "Неправильно введен номер телефона";
            }
        }

        $params = $this->productsAndTotalPrice();
        $params['data'] = $data;
        $params['auth'] = $id;

        if (count($errors)) $params['errors'] = $errors;
        else if(!count($errors)&&!empty($_POST)){
            $order=new Order;
            if($id)$orderId=$order->createUserOrder($id,$params['products']);
            else $orderId=$order->createCustomerOrder($_POST['first_name'],$_POST['phone'],$params['products']);
            $params['orderId']=$orderId;
            $this->cart->clearCart();
        }
        $this->view->render('order/index', $params);
    }




}