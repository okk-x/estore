<?php

namespace App\Controllers;
use App\Models\Product;
use Core\Controller;

final class MainController extends Controller{

    public function __construct()
    {
        parent::__construct();
        $this->view->addScript("slider.js");
    }
    public function actionIndex(){
        $modelProduct=new Product;
        $products=$modelProduct->limit(8)->getProducts();
        if(isset($_COOKIE['viewed_products'])){
            $viewedArr=explode(";",$_COOKIE['viewed_products']);
            $viewed=$modelProduct
                ->id(array_slice(array_reverse($viewedArr),0,8))
                ->getProducts();
        }
        $this->view->render("main/index",compact('products','viewed'));
    }
}