<?php


namespace App\Controllers;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Core\Controller;

final class AccountController extends Controller
{
    private $user;

    public function __construct()
    {
        parent::__construct();
        $this->user=new User;
        if(!$this->user->checkLogged())header('Location:/login');
    }
    public function actionIndex(){
        $userId=$this->user->checkLogged();
        $orderModel = new Order();
        $orders=$orderModel->getOrderByUserId($userId);
        $this->view->render('account/index',compact('orders'));
    }

}