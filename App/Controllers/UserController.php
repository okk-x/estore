<?php

namespace App\Controllers;

use App\Helpers\Validation;
use App\Models\Cart;
use App\Models\User;
use Core\Controller;

final class UserController extends Controller
{
    private $user;
    private $forGuest=['register','login','check-register','check-login'];

    public function __construct()
    {
        parent::__construct();
        $this->user = new User;
        $this->view->addScript("auth.js");
        $this->accessForPage();
    }

    public function accessForPage(){
        $request=trim($_SERVER['REQUEST_URI'],'/');
        if($this->user->isGuest()){
            if(!in_array($request,$this->forGuest)){
                header("Location:/");
            }
        } else{
            if(in_array($request,$this->forGuest)){
                header("Location:/account");
            }
        }
    }
    public function actionRegister()
    {
        $this->view->render("forms/register", compact('errors'));
    }

    public function actionLogin()
    {
        $this->view->render("forms/login", compact('errors'));
    }

    public function actionCheckRegister()
    {
        $data=json_decode(file_get_contents("php://input"),true);
        $errors = [];
        if (!isset($data['first_name'])|| !isset($data['password']) || !isset($data['re-password']) || !isset($data['email']) || !isset($data['phone'])) {
            $errors[] = "Не все поля введены";
        }

        if (!Validation::checkName($data['first_name'])) {
            $errors[] = "Неправильно введено имя";
        }

        if (!Validation::checkEmail($data['email'])) {
            $errors[] = "Неправильно введен email";
        }
        if (!Validation::checkPassword($data['password'])) {
            $errors[] = "Неправильно введен пароль";
        }
        if (!Validation::checkPhone($data['phone'])) {
            $errors[] = "Неправильно введен номер телефона";
        }
        if (strcmp($data['password'], $data['re-password'])) {
            $errors[] = "Пароли не совпадают";
        }
        if (!count($errors)) {
            if (!$this->user->addUser($data['first_name'], $data['email'], $data['phone'], $data['password'])) $errors[] = "Пользователь уже существует";
            else {
                echo json_encode(['status' => 'success']);
                return;
            }
        }
        echo json_encode(['status' => 'error', 'message' => $errors]);
    }

    public function actionCheckLogin()
    {
        $data=json_decode(file_get_contents("php://input"),true);
        $errors = [];
        if (!isset($data['password']) || !isset($data['email'])) {
            $errors[] = "Не все поля введены";
        }
        if (!Validation::checkEmail($data['email'])) {
            $errors[] = "Неправильно введен email";
        }
        if (!Validation::checkPassword($data['password'])) {
            $errors[] = "Неправильно введен пароль";
        }
        if (!count($errors)) {
            if ($userId = $this->user->checkUserData($data['email'], $data['password'])) {
                $this->user->auth($userId);
                echo json_encode(['status' => 'success']);
                return;
            }
            else $errors[] = "Не удалось войти в кабинет";
        }
        echo json_encode(['status' => 'error', 'message' => $errors]);
    }
    public function actionLogout(){
        unset($_SESSION['user']);
        header('Location:/');
    }

}