<?php


namespace App\Controllers;
use App\Models\Review;
use App\Models\User;
use Core\Controller;

class ReviewController extends Controller
{
    private $review;

    public function __construct()
    {
        parent::__construct();
        $this->review=new Review;
    }

    public function actionCreate(){
        list('rating'=>$rating,'product'=>$productId,'text'=>$text)=json_decode(file_get_contents("php://input"),1);
        if(isset($text)){
            $text=htmlspecialchars($text);
        }
        $user=new User;
        if(($userId=$user->checkLogged())&&!$this->review->exists($userId,$productId)){
            $this->review->create($userId,$productId,$rating,$text);
            echo json_encode($this->review->offsetReviews($productId),JSON_UNESCAPED_UNICODE);
        }else echo json_encode(['message'=>'Отзыв уже был добавлен ранее']);
    }

}