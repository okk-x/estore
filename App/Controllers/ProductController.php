<?php

namespace App\Controllers;
use App\Models\Category;
use App\Models\Product;
use App\Models\Review;
use App\Models\User;
use Core\Controller;

final class ProductController extends Controller
{
    public function actionIndex($param)
    {
        $modelProduct=new Product;
        $modelCategory=new Category;
        $modelReview=new Review;
        $modelUser=new User;
        $id = intval($param[0]);
        $modelProduct->id($id);
        if ($product = $modelProduct->getProducts()[0]) {
            if (!isset($_COOKIE['viewed_products'])){
                setcookie('viewed_products', "$id", time() + 60 * 60 * 24 * 5,"/");
            }
            else {
                $viewed = explode(";", $_COOKIE['viewed_products']);
                if (!in_array($id, $viewed)) {
                    $viewed[] = $id;
                    setcookie('viewed_products', implode(";", $viewed),time() + 60 * 60 * 24 * 5,"/");
                }
            }
            $description = [];
            foreach (explode(";", $product['description']) as $desc) {
                if(!empty($desc))
                $description[] = explode(':', $desc);
            }

            $category = $modelCategory->getCategoryById($product['category_id']);
            $reviews=$modelReview->offsetReviews($id);
            $count=$modelReview->count($id);
            $isGuest=$modelUser->isGuest();
            $this->view->render('product/index', compact('product','description', 'category','reviews','isGuest','count'));
        }
        else throw new \Exception();
    }

}