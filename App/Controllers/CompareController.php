<?php


namespace App\Controllers;


use App\Models\Category;
use App\Models\Compare;
use App\Models\Product;
use Core\Controller;
use http\Exception\InvalidArgumentException;

final class CompareController extends Controller
{
    private $compare;

    public function __construct()
    {
        parent::__construct();
        $this->compare = new Compare();
    }

    public function actionIndex()
    {
        if ($list = $this->list()) {
            header("Location:/compare/".$list[0]['id']."/1");
        }
        else $this->view->render('compare/empty');
    }

    public function actionCategory($params)
    {
        list($id,$step)=$params;
        if ($this->compare->countItems() && $this->compare->hasCategory($id)) {
            $list=$this->list();
            foreach($list as &$item){
                if($item['id']==$id)$item['isCurrent']=true;
            }
            $productsId = $this->compare->getItemsByCategoryId($id);
            if(ceil(count($productsId)/3)<$step)throw new \Exception();
            if(ceil(count($productsId)/3)>$step)$next=$step+1;
            if($step!=1)$prev=$step-1;
            $productModel = new Product;
            $products = $productModel->id(array_slice($productsId,$step*3-3,3))->getProducts();
            $common = [];
            $i = 0;
            foreach ($products as &$product) {
                $description = [];
                foreach ($product as $key => $value) {
                    if (!strcmp($key, 'description')) {
                        foreach (explode(";", $value) as $desc) {
                            if (!empty($desc))
                                $arr = explode(':', $desc);
                            if (!in_array($arr[0], $common)) $common[] = $arr[0];
                            $description[$arr[0]] = $arr[1];
                        }
                    }
                }
                $product['description'] = $description;
            }
        }
        else header("Location:/compare");
        $this->view->render('compare/index', compact('list', 'common', 'products','next','prev','id'));
    }

    public function actionAddItem($item)
    {
        $this->compare->addItem($item[0]);
        echo json_encode(['count' => $this->compare->countItems()]);
    }

    public function actionDeleteItem($item)
    {
        $this->compare->deleteItem($item[0]);
        echo json_encode(['count' => $this->compare->countItems()]);
    }

    public function actionCount()
    {
        echo json_encode(['count' => $this->compare->countItems()]);
    }
    public function actionClear(){
        $this->compare->clear();
        header('Location:/');
    }

    public function list()
    {
        if ($items = $this->compare->getItems()) {
            $category = new Category;
            $list = [];
            foreach ($items as $key => $value) {
                $list[] = ['id' => $key, 'category' => $category->getCategoryById($key)['title'], 'count' => count($value)];
            }
            return $list;
        }
        return false;
    }
}