<?php


namespace App\Models;

use Core\Model;
use PDO;

class Cart extends Model
{

    public function addProduct($id)
    {
        $stmt=$this->db->prepare('SELECT EXISTS (SELECT * FROM products WHERE id=:id);');
        $stmt->bindValue(':id',$id,PDO::PARAM_INT);
        $stmt->execute();
        if($result=$stmt->fetch()[0]) {
            if (isset($_SESSION['cart'][$id])) {
                $_SESSION['cart'][$id]['count'] = 1;
            } else {
                $_SESSION['cart'][$id]['count']++;
            }
        }
        else return;
    }

    public function removeProduct($id)
    {
        if (isset($_SESSION['cart'][$id])) unset($_SESSION['cart'][$id]);
    }

    public function productsInCart()
    {
        $result = [];
        foreach ($_SESSION['cart'] as $key => $value) {
            $result[] = ['product_id' => $key, 'count_products' => $value['count']];
        }
        return $result;
    }

    public function clearCart()
    {
        unset($_SESSION['cart']);
    }

    public function changeNumberProduct($id, $number)
    {
        $_SESSION['cart'][$id]['count'] += $number;
    }

    public function count()
    {
        $count['total_count'] = 0;
        if (isset($_SESSION['cart'])) {
            foreach ($_SESSION['cart'] as $item) {
                $count['total_count'] += $item['count'];
            }
        }
        return $count;
    }

}