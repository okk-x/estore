<?php


namespace App\Models;

use Core\Model;
use PDO;

class Review extends Model
{
    public function create($userId,$productId,$rating,$text=null){
        $query="INSERT INTO reviews(`user_id`,`product_id`,`text`,`product_rating`) VALUES(:userId,:productId,:text,:rating)";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(':userId',$userId,PDO::PARAM_INT);
        $stmt->bindValue(':productId',$productId,PDO::PARAM_INT);
        $stmt->bindValue(':rating',$rating,PDO::PARAM_INT);
        $stmt->bindValue(':text',$text,PDO::PARAM_STR);
        return $stmt->execute();
    }
    public function offsetReviews($productId,$start=0,$maxCount=10){
        $query="SELECT t2.first_name AS name,t1.product_rating AS rating,t1.text AS text, DATE_FORMAT(t1.create_at,'%d.%m.%Y') AS publication_date
                FROM reviews t1
                INNER JOIN users t2 ON t1.user_id=t2.id
                WHERE t1.product_id=:id
                ORDER BY t1.create_at DESC
                LIMIT :offset,:limit";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(':id',$productId,PDO::PARAM_INT);
        $stmt->bindValue(':offset',$start,PDO::PARAM_INT);
        $stmt->bindValue(':limit',$maxCount,PDO::PARAM_INT);
        if($stmt->execute()){
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
    }
    public function count($productId){
        $query="SELECT COUNT(*) FROM reviews WHERE product_id=:id";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(':id',$productId,PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch()[0];
    }
    public function exists($userId,$productId){
        $query="SELECT EXISTS(SELECT * FROM reviews WHERE user_id=:userId and product_id=:productId)";
        $stmt=$this->db->prepare($query);
        $stmt->bindValue(':userId',$userId,PDO::PARAM_INT);
        $stmt->bindValue(':productId',$productId,PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch()[0];
    }
}