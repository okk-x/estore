<?php


namespace App\Models;
use Core\Model;
use PDO;

class User extends Model
{
    public function addUser($first_name,$email,$phone,$password){
        $this->db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_SILENT);
        $stmt=$this->db->prepare("INSERT INTO users(`first_name`,`email`,`phone`,`password`) VALUES(:fname,:email,:phone,:password)");
        $stmt->bindValue(':fname',$first_name,PDO::PARAM_STR);
        $stmt->bindValue(':email',$email,PDO::PARAM_STR);
        $stmt->bindValue(':phone',$phone,PDO::PARAM_STR);
        $stmt->bindValue(':password',md5(md5($password)),PDO::PARAM_STR);
        return $stmt->execute();
    }
    public function checkUserData($email,$password){
        $stmt=$this->db->prepare("SELECT * FROM users WHERE email=:email and password=:password");
        $stmt->bindValue(":email",$email,PDO::PARAM_STR);
        $stmt->bindValue(":password",md5(md5($password)),PDO::PARAM_STR);
        if($stmt->execute()){
            return $stmt->fetch()['id'];
        }
        return false;
    }
    public function getUserData($id){
        $result=$this->db->query("SELECT * FROM users WHERE id=$id");
        return $result->fetch(PDO::FETCH_ASSOC);
    }
    public function auth($id){
        $_SESSION['user']['id']=$id;
    }
    public function isGuest(){
        if(isset($_SESSION['user']))return false;
        return true;
    }
    public function checkLogged(){
        if(isset($_SESSION['user']))return $_SESSION['user']['id'];
        return false;
    }
}