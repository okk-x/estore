<?php

namespace App\Models;

use Core\Model;
use http\Exception\InvalidArgumentException;
use PDO;

class Product extends Model
{
    const SORT_POPULAR = 0;
    const SORT_DESC = 1;
    const SORT_ASC = 2;

    protected $id;
    protected $description;
    protected $categoryId;
    protected $brandId;
    protected $range;
    protected $limit;
    protected $offset;

    private function setId(&$variable, $id)
    {
        if (is_array($id)) {
            $variable = [];
            for ($i = 0; $i < count($id); $i++) {
                if (!is_numeric($id[$i])) throw new  \InvalidArgumentException("Element of array must be number in " . __METHOD__);
                $variable[$i] = intval($id[$i]);
            }
        } else {
            if (!is_numeric($id)) throw new \InvalidArgumentException("Params must be number or array of numbers in " . __METHOD__);
            $variable = intval($id);
        }
    }

    public function limit($limit)
    {
        if (!is_int($limit)) throw new \InvalidArgumentException("Argument must be integer in " . __METHOD__);
        $this->limit = $limit;
        return $this;
    }

    public function offset($offset)
    {
        if (!is_int($offset)) throw new \InvalidArgumentException("Argument must be integer in " . __METHOD__);
        $this->offset = $offset;
        return $this;
    }

    public function id($id)
    {
        $this->setId($this->id, $id);
        return $this;
    }

    public function brandId($id)
    {
        $this->setId($this->brandId, $id);
        return $this;
    }

    public function categoryId($id)
    {
        $this->setId($this->categoryId, $id);
        return $this;
    }

    public function description($description)
    {
        $this->description[] = $description;
        return $this;
    }

    public function range($min, $max)
    {
        if (!is_numeric($min) || !is_numeric($max))
            throw new InvalidArgumentException("Arguments must be number in " . __METHOD__);
        else if ($min > $max) throw new InvalidArgumentException("First argument must be less  " . __METHOD__);
        $this->range = [$min, $max];
        return $this;
    }

    protected function where()
    {
        $params = [];

        $temp = "";
        $array = ['id' => $this->id, 'brand_id' => $this->brandId, 'category_id' => $this->categoryId];
        foreach ($array as $key => $val) {
            if (isset($val)) {
                $temp = " $key IN(";
                if (is_array($val)) $temp .= implode(",", $val);
                else $temp .= $val;
                $temp .= ") ";
                $params[] = $temp;
            }
        }

        if (is_array($this->description)) {
            $descriptionString = "";
            for ($i = 0; $i <= count($this->description) - 1; $i++) {
                if (is_array($this->description[$i]['value'])) {
                    //print_r($this->description[$i]['value']);die;
                    $descriptionString .= "(";
                    for ($j = 0; $j <= count($this->description[$i]['value']) - 1; $j++) {
                        $descriptionString .=
                            " description LIKE '%" . $this->description[$i]['name'] . ":" .
                            $this->description[$i]['value'][$j] . "%' ";
                        if ($j != count($this->description[$i]['value']) - 1) {
                            $descriptionString .= " OR ";
                        }
                    }
                    $descriptionString .= ")";
                } else {
                    $descriptionString .=
                        " description LIKE '%" . $this->description[$i]['name'] . ":" .
                        $this->description[$i]['value'] . "%' ";
                }
                if ($i != count($this->description) - 1) {
                    $descriptionString .= " AND ";
                }
            }
            if (!empty($descriptionString)) {
                $params[] = "$descriptionString";
            }
        }
        if (isset($this->range)) {
            $params[] = " price>=" . $this->range[0] . " AND price<=" . $this->range[1] . " ";
        }
        if (!empty($params)) {
            $where = "WHERE";
            for ($i = 0; $i <= count($params) - 1; $i++) {
                $where .= " $params[$i] ";
                if ($i != count($params) - 1) {
                    $where .= " AND ";
                }
            }
        }
        return $where;
    }
    public function getBrandsByCategory($categoryId)
    {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if ($res = $this->db->query("SELECT * FROM brand WHERE id IN (SELECT brand_id FROM products WHERE category_id=$categoryId GROUP BY brand_id)")) {
            $res->setFetchMode(PDO::FETCH_ASSOC);
            return $res->fetchAll();
        }
        return false;
    }

    public function searchForTitle($title){
        $stmt=$this->db->prepare("SELECT id,title,description,price,category_id, available,`number`,rating,`title-image`
                FROM products INNER JOIN product_images ON id=product_id WHERE title LIKE :title");
        $title="%$title%";
        $stmt->bindParam(":title",$title,PDO::PARAM_STR);
        if($result=$stmt->execute()) {
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        return true;
    }

    public function extraParams()
    {
        if (isset($this->limit)) {
            if($res=$this->db->query("SELECT CEIL(COUNT(*)/$this->limit) countPages,
                            MIN(price) minPrice,MAX(price) maxPrice FROM products " . $this->where()))
                return $res->fetch(PDO::FETCH_ASSOC);

        }
        return false;
    }

    public function getProducts($sort = Product::SORT_POPULAR)
    {
        $orderBy = " ORDER BY available DESC, ";
        switch ($sort) {
            case self::SORT_POPULAR:
                $orderBy .= "rating DESC";
                break;
            case self::SORT_ASC:
                $orderBy .= "price ASC";
                break;
            case self::SORT_DESC:
                $orderBy .= "price DESC";
                break;
            default:
                $orderBy = "";
        }
        if (isset($this->limit)) {
            $limit = "LIMIT $this->limit ";
        }
        if (isset($this->offset)) {
            $offset = "OFFSET $this->offset ";
        }
        $query = "SELECT id,title,description,price,category_id, available,`number`,rating,`title-image`
                FROM products INNER JOIN product_images ON id=product_id " . $this->where() . " $orderBy  $limit $offset";
        $res = $this->db->prepare($query);
        $res->setFetchMode(PDO::FETCH_ASSOC);

        if ($res->execute()) {
            return $res->fetchAll();
        }
        return false;

    }
}