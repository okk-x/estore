<?php


namespace App\Models;

use Core\Model;
use PDO;

class Compare extends Model
{


    public function getItems()
    {
        if ($this->countItems())
            return $_SESSION['compare'];
        return false;
    }

    public function hasItem($id)
    {
        if (!$this->countItems()) return false;
        return (boolean)$this->getCategoryIdOfItem($id);
    }
    public function getItemsByCategoryId($id){
        if(isset($_SESSION['compare'][$id])){
            return $_SESSION['compare'][$id];
        }
        return false;
    }
    public function addItem($id)
    {
        if ($this->hasItem($id)) return;
        $stmt = $this->db->prepare('SELECT * FROM products WHERE id=:id');
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        if ($result = $stmt->fetch()) {
            if (!$this->hasCategory($result['category_id'])) {
                $_SESSION['compare'][$result['category_id']] = [];
            }
            $_SESSION['compare'][$result['category_id']][] = $id;
        }
    }
    public function hasCategory($id){
        if(isset($_SESSION['compare'][$id]))return true;
        return false;
    }
    public function getCategoryIdOfItem($id){
        foreach($_SESSION['compare'] as $key=>$category) {
            if(in_array($id, $category))return $key;
        }
        return false;
    }

    public function deleteItem($id)
    {
        if(!$this->hasItem($id))return false;
        if($cat_id=$this->getCategoryIdOfItem($id)){
            if(count($_SESSION['compare'][$cat_id])>1){
                foreach($_SESSION['compare'][$cat_id] as $key=>$val){
                    if($val==$id) unset($_SESSION['compare'][$cat_id][$key]);
                }
            }
            else if(count($_SESSION['compare']))unset($_SESSION['compare'][$cat_id]);
            else unset($_SESSION['compare']);
        }
    }
    public function clear(){
        unset($_SESSION['compare']);
    }

    public function countItems()
    {
        if(isset($_SESSION['compare'])) {
            $count = 0;
            foreach($_SESSION['compare'] as $category){
                $count+=count($category);
            }
            return $count;
        }
        return 0;
    }
}