<?php

namespace App\Models;

use Core\Model;
use PDO;

class Category extends Model
{

    public function getCategoryBySlug($slug)
    {
        $result = $this->db->prepare("SELECT * FROM categories WHERE slug =:slug");
        $result->bindParam(":slug", $slug, PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
    }

    public function getCategoryById($id)
    {
        $id = intval($id);
        $result = $this->db->query("SELECT * FROM categories WHERE id=$id");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
    }

    public function getFilters($id)
    {
        $query="SELECT t1.id, t1.title, t1.value FROM filters t1 INNER JOIN category_filter t2 ON t2.filter_id=t1.id  WHERE t2.category_id=$id ORDER BY title, value";
        if ($result = $this->db->query($query)) {
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $filters = [];
            $i = -1;

            while ($row = $result->fetch()) {
                 if ($i>=0&&!strcmp($row['title'], $filters[$i]['title'])) {
                    $filters[$i]['value'][] = $row['value'];
                    $filters[$i]['id'][]=$row['id'];
                }else {
                    if($i>=0) asort($filters[$i]['value']);
                    $i++;
                    $filters[$i]['title']=$row['title'];
                    $filters[$i]['value']=[];
                    $filters[$i]['value'][]=$row['value'];
                     $filters[$i]['id']=[];
                     $filters[$i]['id'][]=$row['id'];

                }
            }
            return $filters;
        }
        return false;

    }
    public function category(){
        $res=$this->db->query("SELECT id,title,slug FROM categories");
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $categories=$res->fetchAll();
        return $categories;
    }
}