<?php


namespace App\Models;
use Core\Model;
use PDO;

class Order extends Model
{

    public function createUserOrder($id,$products){
        if(!$this->db->query("INSERT INTO orders(`user_id`) VALUES($id)"))return false;
        $orderId=$this->db->lastInsertId();
        $this->insertProductsFromOrder($orderId,$products);
        return $orderId;
    }

    public function createCustomerOrder($name,$phone,$products){
        if(!$this->db->query("INSERT INTO orders(`user_id`) VALUES(NULL)"))return false;
        $orderId=$this->db->lastInsertId();
        $this->db->query("INSERT INTO customer_info VALUES('$name','$phone',$orderId)");
        $this->insertProductsFromOrder($orderId,$products);
        return $orderId;
    }

    private function insertProductsFromOrder($orderId,&$products){
        foreach($products as $product){
            $this->db->query("INSERT INTO order_products VALUES($orderId,".$product['id'].",".$product['count_products'].")");
        }
    }
    public function getOrderByUserId($id){
        $query="SELECT t1.id,t1.status,t1.create_at,t2.product_id,t2.count_products,t3.title,t3.price*t2.count_products as total 
                FROM orders t1 
                INNER JOIN order_products t2 ON t1.id=t2.order_id 
                INNER JOIN products t3 ON t2.product_id=t3.id
                WHERE t1.user_id=$id";
        $result=$this->db->query($query);
        $orders=[];
        while($row=$result->fetch(PDO::FETCH_ASSOC)){
            if(!isset($orders[$row['id']])){
                $orders[$row['id']]=['id'=>$row['id'],'status'=>$row['status'],'create_at'=>$row['create_at']];
            }
            $orders[$row['id']]['products'][]=['product_id'=>$row['product_id'],'title'=>$row['title'],'count_products'=>$row['count_products']];
            $orders[$row['id']]['total']+=$row['total'];
        }
        return $orders;
    }
}