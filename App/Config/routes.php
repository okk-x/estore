<?php
return [
    "categories"=>"category/categoriesList",
    "product/([\d]+)"=>"product/index/$1",
    'search\?query=(.+?)'=>"search/index/$1",
    "cart/count"=>"cart/count",
    "cart/add/([\d]+)"=>"cart/add/$1",
    "cart/remove/([\d]+)"=>"cart/remove/$1",
    "cart/number/([\d]+)/(-?[\d]+)"=>"cart/changeNumber/$1/$2",
    "cart/clear"=>"cart/clear",
    "cart"=>"cart/index",
    'checkout'=>'cart/checkout',
    "confirmed-order"=>"cart/confirmedOrder",
    "login"=>"user/login",
    "register"=>"user/register",
    "logout"=>"user/logout",
    "check-login"=>"user/checkLogin",
    "check-register"=>"user/checkRegister",
    "account"=>"account/index",
    "compare/add/([\d]+)"=>"compare/addItem/$1",
    "compare/delete/([\d]+)"=>"compare/deleteItem/$1",
    "compare/count"=>"compare/count",
    "compare/([\d]+)/([\d]+)"=>'compare/category/$1/$2',
    "compare/clear"=>"compare/clear",
    "compare"=>"compare/index",
    "review/create"=>"review/create",
    "([\w_-]+)/page-([0-9]+)\?filter"=>"category/filter/$1/$2",
    "([\w_-]+)(/page-([0-9]+))?"=>"category/index/$1/$3",
    ''=>"main/index",
];