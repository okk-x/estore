<?php


namespace App\Widgets;
use App\Models\Category;
use Core\Widget;

class NavigationMenu extends Widget
{
    public function run(){
        $categoryModel=new Category;
        $categories=$categoryModel->category();
        $this->render($this->template,compact('categories'));
    }
}