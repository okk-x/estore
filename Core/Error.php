<?php

namespace Core;

class Error{

    public static function exceptionHandler($exception)
    {
        $code=$exception->getCode();
        if ($code != 404) {
            $code = 500;
        }
        $code=404;
        http_response_code($code);

        $log = dirname(__DIR__) . '/logs/' . date('Y-m-d') . '.txt';
        ini_set('error_log', $log);

        $message = "Uncaught exception: '" . get_class($exception) . "'";
        $message .= " with message '" . $exception->getMessage() . "'";
        $message .= "\nStack trace: " . $exception->getTraceAsString();
        $message .= "\nThrown in '" . $exception->getFile() . "' on line " . $exception->getLine();

        error_log($message);

        $view=new MainView();
        $view->render('error/404');
    }

}