<?php

namespace Core;
use App\Controllers;

class Router
{
    private $routes;

    public function __construct()
    {
        $path = ROOT . "/App/Config/routes.php";
        $this->routes = include($path);
    }

    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run()
    {
        $uri = $this->getURI();

        foreach ($this->routes as $route => $path) {
            if (preg_match("|^$route$|", $uri) ) {
                $path = preg_replace("|$route|", $path, $uri);
                $fractions = explode("/", $path);
                $controllerName = 'App\\Controllers\\'.ucfirst(array_shift($fractions)) . "Controller";
                $action = "action" . ucfirst(array_shift($fractions));

                if (!class_exists($controllerName)) {
                    throw new \Exception("Class doesn't exist");
                }

                $controller = new $controllerName();

                if (!method_exists($controller, $action)) {
                    throw new \Exception("$action in " . get_class($controller). " doesn't exist");
                }

                $controller->$action($fractions);
                return;
            }
        }
    }
}