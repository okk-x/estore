<?php

namespace Core;

abstract class Widget extends View
{
    protected $template;

    public function __construct(string $template)
    {
        $this->template = $template;
    }
    public function render($view,$params){
        $file=ROOT."/App/Views/widgets/".$view.".php";
        extract($params);
        if(file_exists($file)) include($file);
    }
    abstract public function run();
}