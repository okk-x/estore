<?php

namespace Core;
use PDO;

abstract class Model
{
    protected $db;
    public function __construct(){
        $this->db=self::getDB();
    }
    private static function getDB()
    {
        static $db;

        if (!$db) {
            $params = include(ROOT . "/app/config/db_params.php");
            $db = new PDO("mysql:host={$params['host']};dbname={$params['dbname']};charset=utf8", $params['user'], $params['password'],[PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return $db;
    }
}