<?php
namespace Core;
use Core\View;

class MainView extends View
{
    public $layout = "main";
    public $scripts=[];
    public function render($view, $params=[])
    {
        $pathview = ROOT . "/App/Views/" . $view . ".php";
        extract($params);
        if (file_exists($pathview))
            include(ROOT . "/App/Views/layouts/" . $this->layout . ".php");
    }

    public function addScript($script){
        if(is_array($script))array_push($this->scripts,$script);
        else $this->scripts[]=$script;
    }
    public function widget($classname,$template){
        $widget=new $classname($template);
        $widget->run();
    }
}