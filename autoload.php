<?php

spl_autoload_register(function ($className){
   $file=ROOT."/".str_replace('\\',"/",$className).".php";
   if(file_exists($file)) include_once($file);
});